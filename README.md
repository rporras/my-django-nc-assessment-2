Part I: Movies App with Django 
Task #1 - Creating a movies Django app
Implement the Django movies application. Create movies and reviews models. 

Create different Django views to cover the acceptance criteria in the assessment context:

List all the movies sorted by date and rating (descending)

Movie’s details views
Create new movie
Delete movies
Edit movies Create new rating. You must calculate the movie’s overall rating based on every user feedback. 

Additionally, Super admins should be able to register new movies into the site using Django’s built-in admin panel. 

 

Task #2 - Testing the Application
We do like testing our code, don’t you?? 

Unit tests have been implemented for you to verify top-notch code has been written. Your task is to ensure that all unit tests pass (without editing any of them). Make sure to write your views as class-based views (CBV).

Constraints: 

Use Django Framework 2.x and Python 3.x to create a movie review app
Only class-based views are allowed
Split applications using Django reusable apps mindset
 