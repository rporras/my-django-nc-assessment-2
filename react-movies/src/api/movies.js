import axios from 'axios';
import cookie from 'react-cookies';


let lookupOptions = {
    baseURL: 'http://localhost:8000/movies/',
    headers: {
        'Content-Type': 'application/json',
    }
}

const csrfToken = cookie.load('csrftoken');
if (csrfToken !== undefined) {
    lookupOptions['credentials'] = 'include'
    lookupOptions['headers']['X-CSRFToken'] = csrfToken
 }

export default axios.create(lookupOptions);