import React from 'react';
import MovieRow from './MovieRow'

const MovieList =  (props) => {
    let result;
    if (props.movies.length > 0) {
        console.log(props.movies);
        const movies = props.movies.map((movie) => <MovieRow key={movie.id} movie={movie} />);
        result = (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th><b>Title</b></th>
                        <th><b>Rating</b></th>
                        <th colSpan="2"><b>Actions</b></th>
                    </tr>
                </thead>
                <tbody>
                    {movies}
                </tbody>
            </table>
        );
    } else {
        result = <div></div>;
    }


    return result;
};

export default MovieList;