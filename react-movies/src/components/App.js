import React from 'react';
import unsplash from '../api/movies';
import SearchBar from './SearchBar';
import MovieList from './MovieList';

class App extends React.Component {

    state = {movies: [] };

    onSearchSubmit = async (term)  => {
        const response = await unsplash.get('/movies/', {
            params: {
                query: term
            }
        });
        console.log(response);
        this.setState({movies: response.data});
    }

    componentDidMount() {
        this.onSearchSubmit();
    }

    render() {
        return (
            <div className="ui container" style={{marginTop: '10px'}}>
                <SearchBar onSubmit={this.onSearchSubmit}/>
                <MovieList movies={this.state.movies}/>
            </div>
        );
    }
}

export default App;