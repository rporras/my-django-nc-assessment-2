import React from 'react';

class ImageCard extends React.Component {

    render () {
        const movie = this.props.movie;
        return (
            <tr>
                <td>{ movie.title }</td>
                <td>{ movie.get_rating }</td>
                <td>Edit</td>
                <td>Delete</td>
            </tr>
        );
    }
}

export default ImageCard;