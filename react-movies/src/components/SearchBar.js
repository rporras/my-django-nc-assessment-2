import React from 'react';

class SearchBar extends React.Component {

    onInputChange(event) {
        console.log(event.target.value);
    }

    onInputClick() {
        console.log("I was clicked")
    }

    // To fix issue with this context
    //onFormSubmit = (event) => {
    onFormSubmit(event) {
        event.preventDefault();

        this.props.onSubmit(this.state.term);
    }

    state={term: ''};


    render() {
        return (
            <div className="row">
                <form className="col-sm-12" onSubmit={(e) => this.onFormSubmit(e)}>
                    <label>Search</label>
                    <input type="text" value={this.state.term} 
                        onClick={this.onInputClick}
                        onChange={(e) => this.setState({term: e.target.value})}/>
                </form>
            </div>
        );
    }
}

export default SearchBar;