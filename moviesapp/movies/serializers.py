from rest_framework import serializers
from .models import Movie, Rating


class MovieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Movie
        fields = ['id','title', 'year', 'rated', 'released_on', 'genre', 
            'director', 'plot', 'get_rating', 'rating_set']

class RatingSerializer(serializers.HyperlinkedModelSerializer):
        model = Rating
        fields = ['id','value', 'comment', 'movie']
