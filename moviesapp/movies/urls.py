# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'movies', views.MovieViewSet)
router.register(r'ratings', views.RatingViewSet)

app_name = 'moviesapp.movies'

urlpatterns = [
    url(r'^$', view=views.MovieListView.as_view(), name='index'),
    url(r'^(?P<pk>[\d]+)/$', view=views.MovieDetailView.as_view(), name='detail'),
    url(r'^create/$', view=views.MovieCreateView.as_view(), name='create'),
    url(r'^update/(?P<pk>[\d]+)/$', view=views.MovieUpdateView.as_view(), name='update'),
    url(r'^delete/(?P<pk>[\d]+)/$', view=views.MovieDeleteView.as_view(), name='delete'),
    url(r'^(?P<moviedId>[\d]+)/createRating/$', view=views.RatingCreateView.as_view(), name='createRating'),
    url(r'^(?P<moviedId>[\d]+)/updateRating/(?P<pk>[\d]+)/$', view=views.RatingUpdateView.as_view(), name='updateRating'),
    path('', include(router.urls))
]
