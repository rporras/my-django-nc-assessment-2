# -*- coding: utf-8 -*-

"""Movies views."""

from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib import messages
from django.shortcuts import redirect, get_object_or_404
from django.http import Http404
from django.urls import reverse_lazy, reverse
from django.db.models import Avg
from rest_framework import viewsets

from .models import Movie, Rating
from .serializers import MovieSerializer, RatingSerializer


class MovieListView(ListView):
    """Show all movies."""
    model = Movie


    def get_queryset(self):
        if 'title' in self.request.GET.keys():
            movies = Movie.objects.filter(title__startswith=self.request.GET['title']).annotate(average_rating = Avg('rating__value')).order_by('-released_on', 'average_rating')
        else:
            movies = Movie.objects.annotate(average_rating = Avg('rating__value')).order_by('-released_on', 'average_rating')

        return movies


class MovieDetailView(DetailView):
    """Show the requested movie."""
    model = Movie


class MovieCreateView(CreateView):
    """Create a new movie."""
    model = Movie
    fields = ['title', 'year', 'rated', 'released_on', 'genre', 
            'director', 'plot']


class MovieUpdateView(UpdateView):
    """Update the requested movie."""
    model = Movie
    fields = ['title', 'year', 'rated', 'released_on', 'genre', 
            'director', 'plot']


class MovieDeleteView(DeleteView):
    """Delete the requested movie."""
    model = Movie
    success_url = reverse_lazy('movies:index')

class RatingCreateView(CreateView):
    """Create a new movie."""
    model = Rating
    fields = ['value', 'comment']

    def get_context_data(self, **kwargs):
        """ Get context variables for rendering the template. """
        kwargs['movie'] = self.movie
        return super().get_context_data(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        """
        Overridden so we can make sure the `Movie` instance exists
        before going any further.
        """
        self.movie = get_object_or_404(Movie, pk=kwargs['moviedId'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """
        Overridden to add the movie relation to the `Rating` instance.
        """
        form.instance.movie = self.movie
        return super().form_valid(form)


class RatingUpdateView(UpdateView):
    """Update the requested movie."""
    model = Rating
    fields = ['value', 'comment']

    def get_context_data(self, **kwargs):
        """ Get context variables for rendering the template. """
        kwargs['movie'] = self.movie
        return super().get_context_data(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        """
        Overridden so we can make sure the `Movie` instance exists
        before going any further.
        """
        self.movie = get_object_or_404(Movie, pk=kwargs['moviedId'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        """
        Overridden to add the movie relation to the `Rating` instance.
        """
        form.instance.movie = self.movie
        return super().form_valid(form)

class MovieViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows movies to be viewed or edited
    """
    queryset = Movie.objects.annotate(average_rating = Avg('rating__value')).order_by('-released_on', 'average_rating')
    serializer_class = MovieSerializer


class RatingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Rating to be viewed or edited
    """
    queryset = Rating.objects.all()
    serializer_class = RatingSerializer